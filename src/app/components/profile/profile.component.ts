import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {HttpClient} from "@angular/common/http";
import {AdminService} from "../../services/admin.service";
import {Admin} from "../../interfaces/admin";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  deleteMessage=false;
  TAdmin: Admin[]=[];
  updateadminform!: FormGroup;
  adminToUpdate!: Admin;
  isupdated = false;
  validMessage: string = "";
  constructor(private http: HttpClient,private adminService:AdminService) { }

  ngOnInit(): void {
    this.adminService.getAdmin().subscribe(data =>{
    })
  }
  //update begain
  getToUpdateAdmin(id: number) {
    this.adminService.getToUpdateadmin(id)
      .subscribe(
        data => {
          this.adminToUpdate = data ,

            console.log(this.adminToUpdate.admin_BIRTHDATE)
        },
        error => console.log(error));
  }
  adminupdateform = new FormGroup({
    admin_ID: new FormControl(''),
    admin_NAME: new FormControl(''),
    admin_LASTNAME: new FormControl(''),
    admin_BIRTHDATE:new FormControl(''),
    admin_GENDER: new FormControl(''),
    admin_ADRESSE:new FormControl(''),
    admin_NUM:new FormControl(''),
    admin_TELE: new FormControl(''),
    admin_EMAIL:new FormControl(''),
    admin_BYTE:new FormControl(''),
    admin_LOGIN: new FormControl(''),
    admin_PASSWORD: new FormControl(''),
  });
  updateAdmin(){
    console.log(this.adminupdateform.value);
    this.adminService.updateadmin(this.adminToUpdate.admin_ID,this.adminupdateform.value).subscribe(
      data => {
        this.isupdated=true;
        this.adminService.getAdmin().subscribe(data =>{
          this.TAdmin = data
        })
      },
      error => console.log(error));
  }
  get admin_ID(){
    return this.adminupdateform.get('admin_ID');
  }
  get admin_NAME(){
    return this.adminupdateform.get('admin_NAME');
  }
  get admin_LASTNAME(){
    return this.adminupdateform.get('admin_LASTNAME');
  }
  get admin_LOGIN() {
    return this.adminupdateform.get('admin_LOGIN');
  }
  get admin_PASSWORD(){
    return this.adminupdateform.get('admin_PASSWORD');
  }
  get admin_BIRTHDATE(){
    return this.adminupdateform.get('admin_BIRTHDAY');
  }
  get admin_GENDER(){
    return this.adminupdateform.get('admin_GENDER');
  }
  get admin_ADRESSE(){
    return this.adminupdateform.get('admin_ADRESSE');
  }
  get admin_NUM() {
    return this.adminupdateform.get('admin_NUM');
  }
  get admin_TELE(){
    return this.adminupdateform.get('admin_TELE');
  }
  get admin_BYTE() {
    return this.adminupdateform.get('admin_BYTE');
  }
  get admin_EMAIL(){
    return this.adminupdateform.get('admin_EMAIL');
  }

  changeisUpdate(){
    this.isupdated=false;
  }
//update end
}
