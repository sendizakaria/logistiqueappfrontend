import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';
import {Order} from "../../interfaces/order";
import {FormControl, FormGroup} from "@angular/forms";
import {HttpClient} from "@angular/common/http";
import {OrderService} from "../../services/order.service";
import {Facture} from "../../interfaces/facture";
import {FactureService} from "../../services/facture.service";
import {Chauffeur} from "../../interfaces/chauffeur";

@Component({
  selector: 'app-facture',
  templateUrl: './facture.component.html',
  styleUrls: ['./facture.component.scss']
})
export class FactureComponent implements OnInit {

  deleteMessage=false;
  TFacture: Facture[]=[];
  factureform!: FormGroup;
  validMessage: string = "";
  factureToUpdate!: Facture;
  isupdated = false;
  fact!:Facture;
  constructor(private http: HttpClient, private router:Router,private factuService:FactureService,private appComponent: AppComponent) { }

  ngOnInit(): void {
    this.factureform = new FormGroup({
      facture_ID: new FormControl(''),
      order_ID: new FormControl(''),
      company_NAME: new FormControl(''),
      order_DATE_EDITION: new FormControl(''),
      facture_DATE: new FormControl(''),
      facture_TOTALE: new FormControl(''),
      facture_STATE: new FormControl(''),
      fac_BYTE: new FormControl(''),
    })
    this.factuService.getFacture().subscribe(data =>{
    })
    this.getAllFactures();
    console.log('all orders qre Loaded',this.TFacture);
  }
  getAllFactures(){
    this.factuService.getFacture().subscribe(
      data =>{this.TFacture = data},
      err =>console.error(err),
      ()=> console.log('all trips Loaded')
    )
  }
  enregistrerFacture(){
    console.log('save client',this.factureform.value);
    if(this.factureform.valid ){
      console.log('suivieform valid',this.factureform.value);

      this.factuService.createFacture(this.factureform.value).subscribe(
        data=>{
          this.getAllFactures();
          return true;
          this.validMessage= "Your client registration has been submitted. \t Thank you ! ";
        },
        error=>{
          throw new Error("Please fill out the form before submitting !");  }
      )
    }else {
      this.validMessage= "Please fill out the form before submitting !";

    }
  }
  //update begain
  getToUpdatefactures(id: number) {
    this.factuService.getToUpdatefacture(id)
      .subscribe(
        data => {
          this.factureToUpdate = data ,

            console.log(this.factureToUpdate.facture_ID)
        },
        error => console.log(error));
  }
 factureupdateform = new FormGroup({
   facture_ID: new FormControl(''),
   order_ID: new FormControl(''),
   company_NAME: new FormControl(''),
   order_DATE_EDITION: new FormControl(''),
   facture_DATE: new FormControl(''),
   facture_TOTALE: new FormControl(''),
   facture_STATE: new FormControl(''),
   fac_BYTE: new FormControl(''),
  });

  updateFacture(){
    console.log(this.factureupdateform.value);
    this.factuService.updatefacture(this.factureToUpdate.facture_ID,this.factureupdateform.value).subscribe(
      data => {
        this.isupdated=true;
        this.factuService.getFacture().subscribe(data =>{
          this.TFacture = data
        })
      },
      error => console.log(error));
  }
  get facture_ID(){
    return this.factureupdateform.get('facture_ID');
  }
  get order_ID(){
    return this.factureupdateform.get('order_ID');
  }
  get company_NAME(){
    return this.factureupdateform.get('company_NAME');
  }
  get order_DATE_EDITION() {
    return this.factureupdateform.get('order_DATE_EDITION');
  }
  get facture_DATE(){
    return this.factureupdateform.get('facture_DATE');
  }

  get facture_TOTALE(){
    return this.factureupdateform.get('facture_TOTALE');
  }
  get facture_STATE(){
    return this.factureupdateform.get('facture_STATE');
  }
  get fac_BYTE(){
    return this.factureupdateform.get('fac_BYTE');
  }

  changeisUpdate(){
    this.isupdated=false;
  }
//update end
  DeleteFacture(id: number) {
    this.factuService.deleteFacture(id)
      .subscribe(
        data => {
          console.log(data);
          this.deleteMessage=true;
          this.getAllFactures();
        },
        error => console.log(error));
  }
  creefacture(id:number):void
  {
    this.factuService.getToUpdatefacture(id)
      .subscribe(
        data => {
          this.fact = data ,

            console.log(this.fact.facture_ID)
        },
        error => console.log(error));
    this.appComponent.showlogin = this.appComponent.showlogin ? false:false;
    this.router.navigate(['/cree-f'],{state:{example:this.fact}});
  }

}
