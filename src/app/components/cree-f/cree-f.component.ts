import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AppComponent} from 'src/app/app.component';
import {CustomerService} from "../../services/customer.service";
import {Client} from "../../interfaces/client";
import {FormGroup} from "@angular/forms";
import {OrderService} from "../../services/order.service";
import {Order} from "../../interfaces/order";
import {FactureService} from "../../services/facture.service";
import {Facture} from "../../interfaces/facture";
import {Observable} from "rxjs";
import {parseArguments} from "@angular/cli/models/parser";

@Component({
  selector: 'app-cree-f',
  templateUrl: './cree-f.component.html',
  styleUrls: ['./cree-f.component.scss']
})
export class CreeFComponent implements OnInit {
  opdateshow:boolean =false;
  factdateshow:boolean =false;
  numshow:boolean =false;
  companyvalue :boolean =true;
  comanytext:boolean = false;
  companyToshow!: Client;
  clientshowform!: FormGroup;
  orderToshow!: Order;
  factdate:boolean =true;
  opdate:boolean =true;
  num:boolean =true;
  f!:Facture;
  idFacture : number | undefined;
  idOrder : number =0;
  factureForm!: Facture | undefined;
  orderForm!: Order | undefined;
  errorMessage = '';
  models: string[] = [
    'ddd',
    'asdasdas',
    'comp1',
    'comp2'
  ];
  constructor(private route:ActivatedRoute,private customerService: CustomerService,private factuService:FactureService,private orderService:OrderService,
    private router:Router,  private appComponent: AppComponent ) {

    }
  ngOnInit(): void {
    this.idFacture = Number(this.route.snapshot.paramMap.get('id'));
    if (this.idFacture) {
      this.getFactureById(this.idFacture);
      console.log("getFactureById"+this.factureForm);
    }
  }

  getFactureById(id :number){
    this.factuService.getFactureById(id)
      .subscribe(
        data => {
           // @ts-ignore
          this.factureForm = data , this.idOrder = this.factureForm.order_ID
          this.getOrderById(this.idOrder)
         },
        error => console.log(error));

  }

  getOrderById(id: number  ){
    this.orderService.getOrderById(id)
      .subscribe(
        data => {
          this.orderForm = data
        },
        error => console.log(error));

  }

/*

  facture():void
  {
    this.appComponent.showlogin = this.appComponent.showlogin ? false:false;
    this.router.navigate(['/facture']);
  }

  makechoice(name:string)
  {
    this.customerService.getToshowClient(name)
      .subscribe(
        data => {
          this.companyToshow = data ,
            console.log(this.companyToshow.total_ORDERS)
        },
        error => console.log(error));
    if(this.companyToshow!=null)
    {
      this.f= (<Facture>this.router.getCurrentNavigation()?.extras.state);
      this.companyvalue = false;
    this.comanytext =true ;
    }
  }
  getToUpdateOrder(id: number) {
    this.orderService.getToUpdateOrders(id)
      .subscribe(
        data => {
          this.orderToshow = data ,

            console.log(this.orderToshow.order_state)
        },
        error => console.log(error));
  }
  dateo(){
    this.opdate=false;
    this.opdateshow = true;
    }
  numf(){
    this.num=false;
    this.numshow = true;}
  datef(){
    this.factdate=false;
    this.factdateshow= true;
  }
  /*
 ngOnDestroy() {
 this.dataservice.facture1 = this.fact;
 }     */
  // this.f= this.dataservice.facture1 ;
}
