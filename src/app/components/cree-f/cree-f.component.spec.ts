import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreeFComponent } from './cree-f.component';

describe('CreeFComponent', () => {
  let component: CreeFComponent;
  let fixture: ComponentFixture<CreeFComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreeFComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreeFComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
