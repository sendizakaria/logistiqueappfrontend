import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {DriverService} from "../../services/driver.service";
import {AppComponent} from "../../app.component";
import {FormControl, FormGroup} from "@angular/forms";
import {FactureService} from "../../services/facture.service";
import {MessageService} from "../../services/message.service";
import {Facture} from "../../interfaces/facture";
import {Message} from "../../interfaces/message";
import {Time} from "highcharts/highcharts.src";

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss']
})
export class MessageComponent implements OnInit {
  messageform!: FormGroup;
  TMessage: Message[]=[];
  deleteMessage=false;
  validMessage: string = "";
  isupdated = false;
  constructor(private route:ActivatedRoute,
              private router:Router, private appComponent: AppComponent,private messageService:MessageService) { }

  ngOnInit(): void {

    this.messageform = new FormGroup({
    admin_ID: new FormControl(''),
      chauffeur_NAME: new FormControl(''),
      message_id: new FormControl(''),
      message_TEXT: new FormControl(''),
      message_DATE: new FormControl(''),
      message_TIME: new FormControl(''),
    })
    this.messageService.getMessage().subscribe(data =>{
    })
    this.getAllMessage();
    console.log('all MESSAGE qre Loaded',this.TMessage);
  }
  getAllMessage(){
    this.messageService.getMessage().subscribe(
      data =>{this.TMessage = data},
      err =>console.error(err),
      ()=> console.log('all trips Loaded')
    )
  }
  enregistrerMessage(){
    console.log('save MESSAGE',this.messageform.value);
    if(this.messageform.valid ){
      console.log('MESSAGEform valid',this.messageform.value);

      this.messageService.createMessage(this.messageform.value).subscribe(
        data=>{
          this.getAllMessage();
          return true;
          this.validMessage= "Your client registration has been submitted. \t Thank you ! ";
        },
        error=>{
          throw new Error("Please fill out the form before submitting !");  }
      )
    }else {
      this.validMessage= "Please fill out the form before submitting !";

    }
  }
  messagerie():void
  {
    this.appComponent.showlogin = this.appComponent.showlogin ? false:false;
    this.router.navigate(['/messagerie']);
  }
}
