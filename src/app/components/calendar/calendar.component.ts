import { Component, OnInit } from '@angular/core';
import {CalendarOptions} from "@fullcalendar/angular";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit {


  posts = [];
  calendarOptions: CalendarOptions | undefined;
  constructor(private http: HttpClient) { }
  handleDateClick(arg: { dateStr: string; }) {
    alert('date click! ' + arg.dateStr)

  }
  ngOnInit(){
    this.calendarOptions = {
      initialView: 'dayGridMonth',
      dateClick: this.handleDateClick.bind(this), // bind is important!
      events: this.posts
    };
  }
}
