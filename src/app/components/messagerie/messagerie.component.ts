import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {DriverService} from "../../services/driver.service";
import {Chauffeur} from "../../interfaces/chauffeur";
import {AppComponent} from "../../app.component";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-messagerie',
  templateUrl: './messagerie.component.html',
  styleUrls: ['./messagerie.component.scss']
})
export class MessagerieComponent implements OnInit {
  TDriver: Chauffeur[]=[];
  driverform!: FormGroup;

  constructor(private route:ActivatedRoute,
              private router:Router,private driverService:DriverService, private appComponent: AppComponent,) { }

  ngOnInit(): void {
    this.driverform = new FormGroup({
      chauffeur_ID: new FormControl(''),
      chauffeur_NAME: new FormControl(''),
      chauffeur_LASTNAME: new FormControl(''),
      chauffeur_BYTE: new FormControl(''),
      chauffeur_BIRTH_DATE: new FormControl(''),
      chauffeur_LOGIN: new FormControl(''),
      chauffeur_PASSWORD: new FormControl(''),
      chauffeur_DATEENTREE: new FormControl(''),
      chauffeur_SPECIALITY: new FormControl(''),
      chauffeur_STATE: new FormControl(''),
    })
    this.driverService.getChauffeur().subscribe(data =>{
    })
    this.getallChauffeurs();
    console.log('all orders qre Loaded',this.TDriver);
  }

getallChauffeurs(){
  this.driverService.getChauffeur().subscribe(
    data =>{this.TDriver = data},
    err =>console.error(err),
    ()=> console.log('all trips Loaded')
  )
}
  message():void
  {
    this.appComponent.showlogin = this.appComponent.showlogin ? false:false;
    this.router.navigate(['/message']);
  }
}
