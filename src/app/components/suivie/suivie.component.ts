import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {HttpClient} from "@angular/common/http";
import {Suivie} from "../../interfaces/suivie";
import {SuivieService} from "../../services/suivie.service";

@Component({
  selector: 'app-suivie',
  templateUrl: './suivie.component.html',
  styleUrls: ['./suivie.component.scss']
})
export class SuivieComponent implements OnInit {

  deleteMessage=false;
  TSuivie: Suivie[]=[];
  suivieToUpdate!: Suivie;
  isupdated = false;
  suivieform!: FormGroup;
  validMessage: string = "";

  constructor(private http: HttpClient,private suivieService:SuivieService) { }

  ngOnInit(): void {
    this.suivieform = new FormGroup({
      suivie_ID: new FormControl(''),
      suivie_CLIENT_NAME: new FormControl(''),
      driver_NAME: new FormControl(''),
      driver_GSM: new FormControl(''),
      suivie_Ville_DEPART: new FormControl(''),
      suivie_Ville_DESTINATION: new FormControl(''),
      suivie_DATE_Depart: new FormControl(''),
      suivie_DATE_Arrivee: new FormControl(''),
      dispo_CMR: new FormControl(''),
      suivie_State: new FormControl(''),
      arrival_TIME_LOADING: new FormControl(''),
      loading_TIME: new FormControl(''),
      arrival_TIME_UNLOADING: new FormControl(''),
      unloading_TIME: new FormControl(''),
      state_CMR: new FormControl(''),
    })
    this.suivieService.getSuivie().subscribe(data =>{
    })
    this.getAllSuivies();
    console.log('all Suivie qre Loaded',this.TSuivie);
  }
  getAllSuivies(){
    this.suivieService.getSuivie().subscribe(
      data =>{this.TSuivie = data},
      err =>console.error(err),
      ()=> console.log('all trips Loaded')
    )
  }
  enregistrerSuivies(){
    console.log('save client',this.suivieform.value);
    if(this.suivieform.valid ){
      console.log('suivieform valid',this.suivieform.value);

      this.suivieService.createSuivie(this.suivieform.value).subscribe(
        data=>{
          this.getAllSuivies();
          return true;
          this.validMessage= "Your client registration has been submitted. \t Thank you ! ";
        },
        error=>{
          throw new Error("Please fill out the form before submitting !");  }
      )
    }else {
      this.validMessage= "Please fill out the form before submitting !";

    }
  }
  //update begain
  getToUpdateSuivie(id: number) {
    this.suivieService.getToUpdateSuivie(id)
      .subscribe(
        data => {
          this.suivieToUpdate = data ,

            console.log(this.suivieToUpdate.suivie_ID)
        },
        error => console.log(error));
  }
  suiviesupdateform = new FormGroup({
    suivie_ID: new FormControl(''),
    suivie_CLIENT_NAME: new FormControl(''),
    driver_NAME: new FormControl(''),
    driver_GSM: new FormControl(''),
    suivie_Ville_DEPART: new FormControl(''),
    suivie_Ville_DESTINATION: new FormControl(''),
    suivie_DATE_Depart: new FormControl(''),
    suivie_DATE_Arrivee: new FormControl(''),
    dispo_CMR: new FormControl(''),
    suivie_State: new FormControl(''),
    arrival_TIME_LOADING: new FormControl(''),
    loading_TIME: new FormControl(''),
    arrival_TIME_UNLOADING: new FormControl(''),
    unloading_TIME: new FormControl(''),
    state_CMR: new FormControl(''),
  });

  updateSuivie(){
    console.log(this.suiviesupdateform.value);
    this.suivieService.updateSuivie(this.suivieToUpdate.suivie_ID,this.suiviesupdateform.value).subscribe(
      data => {
        this.isupdated=true;
        this.suivieService.getSuivie().subscribe(data =>{
          this.TSuivie = data
        })
      },
      error => console.log(error));
  }
  get suivie_ID(){
    return this.suiviesupdateform.get('suivie_ID');
  }
  get suivie_CLIENT_NAME(){
    return this.suiviesupdateform.get('suivie_CLIENT_NAME');
  }
  get driver_NAME(){
    return this.suiviesupdateform.get('driver_NAME');
  }
  get driver_GSM() {
    return this.suiviesupdateform.get('driver_GSM');
  }
  get suivie_Ville_DEPART(){
    return this.suiviesupdateform.get('suivie_Ville_DEPART');
  }

  get suivie_Ville_DESTINATION(){
    return this.suiviesupdateform.get('suivie_Ville_DESTINATION');
  }

  get suivie_DATE_Depart(){
    return this.suiviesupdateform.get('suivie_DATE_Depart');
  }
  get suivie_DATE_Arrivee(){
    return this.suiviesupdateform.get('suivie_DATE_Arrivee');
  }
  get dispo_CMR(){
    return this.suiviesupdateform.get('dispo_CMR');
  }
  get suivie_State(){
    return this.suiviesupdateform.get('suivie_State');
  }
  get arrival_TIME_LOADING() {
    return this.suiviesupdateform.get('arrival_TIME_LOADING');
  }
  get loading_TIME(){
    return this.suiviesupdateform.get('loading_TIME');
  }

  get arrival_TIME_UNLOADING(){
    return this.suiviesupdateform.get('arrival_TIME_UNLOADING');
  }
  get unloading_TIME(){
    return this.suiviesupdateform.get('unloading_TIME');
  }
  get state_CMR(){
    return this.suiviesupdateform.get('state_CMR');
  }

  changeisUpdate(){
    this.isupdated=false;
  }
//update end
  DeleteSuivie(id: number) {
    this.suivieService.deleteSuivie(id)
      .subscribe(
        data => {
          console.log(data);
          this.deleteMessage=true;
          this.getAllSuivies();
        },
        error => console.log(error));
  }
}
