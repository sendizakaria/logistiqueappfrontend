import { Component, OnInit } from '@angular/core';
import {Chauffeur} from "../../interfaces/chauffeur";
import {FormControl, FormGroup} from "@angular/forms";
import {HttpClient} from "@angular/common/http";
import {DriverService} from "../../services/driver.service";
import {Vheicule} from "../../interfaces/vheicule";
import {AutoService} from "../../services/auto.service";
import {Charges} from "../../interfaces/charges";

@Component({
  selector: 'app-autos',
  templateUrl: './autos.component.html',
  styleUrls: ['./autos.component.scss']
})
export class AutosComponent implements OnInit {

  deleteMessage=false;
  TAuto: Vheicule[]=[];
  autoform!: FormGroup;
  validMessage: string = "";
  autoToUpdate!: Vheicule;
  isupdated = false;
  constructor(private http: HttpClient,private autoService:AutoService) { }

  ngOnInit(): void {
    this.autoform = new FormGroup({
      vheicule_ID: new FormControl(''),
      vheicule_BRAND: new FormControl(''),
      vheicule_MODEL: new FormControl(''),
      vheicule_TYPE: new FormControl(''),
      vheicule__BYTE: new FormControl(''),
      vheicule_STATE: new FormControl(''),
    })
    this.getallAutos();
    console.log('all orders qre Loaded',this.TAuto);
  }
  getallAutos(){
    this.autoService.getAuto().subscribe(
      data =>{this.TAuto = data},
      err =>console.error(err),
      ()=> console.log('all trips Loaded')
    )
  }
  enregistrerAutos(){
    console.log('save client',this.autoform.value);
    if(this.autoform.valid ){
      console.log('suivieform valid',this.autoform.value);

      this.autoService.createAuto(this.autoform.value).subscribe(
        data=>{
          this.getallAutos();
          return true;
          this.validMessage= "Your client registration has been submitted. \t Thank you ! ";
        },
        error=>{
          throw new Error("Please fill out the form before submitting !");  }
      )
    }else {
      this.validMessage= "Please fill out the form before submitting !";
    }
  }
  //update begain
  getToUpdateAuto(id: number) {
    this.autoService.getToUpdateauto(id)
      .subscribe(
        data => {
          this.autoToUpdate = data ,
            console.log(this.autoToUpdate.vheicule_ID)
        },
        error => console.log(error));
  }
  vheiculeupdateform = new FormGroup({
    vheicule_ID: new FormControl(''),
    vheicule_BRAND: new FormControl(''),
    vheicule_MODEL: new FormControl(''),
    vheicule_TYPE: new FormControl(''),
    vheicule__BYTE: new FormControl(''),
    vheicule_STATE: new FormControl(''),
  });

  updateAuto(){
    console.log(this.vheiculeupdateform.value);
    this.autoService.updateAuto(this.autoToUpdate.vheicule_ID,this.vheiculeupdateform.value).subscribe(
      data => {
        this.isupdated=true;
        this.autoService.getAuto().subscribe(data =>{
          this.TAuto = data
        })
      },
      error => console.log(error));
  }
  get vheicule_ID(){
    return this.vheiculeupdateform.get('vheicule_ID');
  }
  get vheicule_BRAND(){
    return this.vheiculeupdateform.get('vheicule_BRAND');
  }
  get vheicule_MODEL(){
    return this.vheiculeupdateform.get('vheicule_MODEL');
  }
  get vheicule_TYPE() {
    return this.vheiculeupdateform.get('vheicule_TYPE');
  }
  get vheicule__BYTE(){
    return this.vheiculeupdateform.get('vheicule__BYTE');
  }
  get vheicule_STATE(){
    return this.vheiculeupdateform.get('vheicule_STATE');
  }
  changeisUpdate(){
    this.isupdated=false;
  }
//update end
  DeleteAuto(id: number) {
    this.autoService.deleteAuto(id)
      .subscribe(
        data => {
          console.log(data);
          this.deleteMessage=true;
          this.getallAutos();
        },
        error => console.log(error));
  }

}
