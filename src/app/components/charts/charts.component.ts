import {Component, OnInit} from '@angular/core';
import * as Highcharts from 'highcharts';
import {FormControl, FormGroup} from "@angular/forms";
import {Charges} from "../../interfaces/charges";
import {HttpClient} from "@angular/common/http";
import {ChargeService} from "../../services/charge.service";

declare var require: any;
@Component({
  selector: 'app-charts',
  templateUrl: './charts.component.html',
  styleUrls: ['./charts.component.scss']
})


export class ChartsComponent implements OnInit {
  highcharts = Highcharts;
  deleteMessage = false;
  Tcharges: Charges[] = [];
  TchargeS!: Charges;
  chargesform!: FormGroup;
  validMessage: string = "";
  chargeToUpdate!: Charges;
  isupdated = false;
  a0=20;
  a1=10;
  i!:number;

  constructor(private http: HttpClient, private chargeService: ChargeService) {
  }
  ngOnInit(): void {
    this.chargesform = new FormGroup({
      charge_ID: new FormControl(''),
      charge_TYPE: new FormControl(''),
      charge_DATE: new FormControl(''),
      charge_STATE: new FormControl(''),
      charge_PRICE: new FormControl(''),
    })
    this.chargeService.getCharges().subscribe(data =>{
    })
    this.getAllCharges();
    console.log('all orders qre Loaded',this.Tcharges);
    Highcharts.chart('container', this.options);
    Highcharts.chart('bar_graphe', this.optionsbar);
    Highcharts.chart('pie_graphe', this.optionspie);
  }
  getAllCharges(){
    this.chargeService.getCharges().subscribe(
      data =>{this.Tcharges = data
        this.a0= this.Tcharges[0].charge_PRICE;
        this.a1=this.Tcharges[1].charge_PRICE;
        console.log('FIRST',this.a0)
        console.log('FIRST',this.a1)
      },
      err =>console.error(err),
      ()=> console.log('all customers Loaded')
    )
    console.log('SECOND',this.a0)
    console.log('SECOND',this.a1)
  }

  public options: any = {
    Chart: {
      type: 'area',
      height: 700
    },
    title: {
      text: 'Analyse of charges'
    },
    credits: {
      enabled: false
    },
    xAxis: {
      categories: ['1750', '1800', '1850', '1900', '1950', '1999', '2050'],
      tickmarkPlacement: 'on',
      title: {
        enabled: false
      }
    },
    series: [
      {
      name: 'taxe',
         data: [677, 635, 809, 947, 1402, 3634, 5268]
       },
      {
      name: 'locale',
      data: [163, 203, 276, 408, 547, 729, 628]
      },
      {
      name: 'cars',
        data:
          [
          this.a0,
            this.a1,
            this.a0, this.a1, this.a0, 8000, 9000
          ]
  }]
  }


  public optionsbar: any = {
    chart: {
      type: 'bar'
    },
    title: {
      text: 'Orders Evolution '
    },
    xAxis: {
      categories: ['2018', '2019', '2020', '2021', '2022'],
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Population (millions)',
        align: 'high'
      },
    },
    tooltip: {
      valueSuffix: ' millions'
    },
    plotOptions: {
      bar: {
        dataLabels: {
          enabled: true
        }
      }
    },
    series: [{
      type: undefined,
      name: 'remorque',
      data: [107, 31, 635, 203, 2]
    }, {
      type: undefined,
      name: 'T1',
      data: [133, 156, 947, 408, 6]
    }, {
      type: undefined,
      name: 'T2',
      data: [814, 841, 3714, 727, 31]
    }, {
      type: undefined,
      name: 'T3',
      data: [1216, 1001, 4436, 738, 40]
    },
{
  type: undefined,
  name: 'T4',
  data: [2216, 901, 4636, 938, 100]
}]
  };


  public optionspie: any = {

    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie'
    },
    title: {
      text: 'profit split'
    },
    tooltip: {
      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}</b>: {point.percentage:.1f} %'
        }
      }
    },
    series: [{
      name: 'Brands',
      colorByPoint: true,
      type: undefined,
      data: [{
        name: 'benifice',
        y: 51,
        sliced: true,
        selected: true
      },
        {
          name: 'invistisement',
          y: 10
        },{
        name: 'locale',
        y: 5
      }, {
        name: 'taxe',
        y: 30
      }, {
        name: 'others',
        y: 14.
      },
       ]
    }]
  };
}
