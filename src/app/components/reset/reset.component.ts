import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';
import {AdminService} from "../../services/admin.service";
import {Admin} from "../../interfaces/admin";
import {FormControl, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-reset',
  templateUrl: './reset.component.html',
  styleUrls: ['./reset.component.scss']
})
export class ResetComponent implements OnInit {
  showlogin: boolean = true;
  adminToUpdate!: Admin;
  TAdmin: Admin[] = [];
  admin: Admin[] = []
  isupdated = false;
  i!: number;

  constructor(private route: ActivatedRoute,
              private router: Router, private appComponent: AppComponent, private adminService: AdminService) {
  }

  ngOnInit(): void {
  }

  welcome(): void {
    this.appComponent.showlogin = this.appComponent.showlogin ? false : false;
    this.router.navigate(['/welcome']);
  }

  resetform = new FormGroup({
    admin_EMAIL: new FormControl(''),
    admin_PASSWORD: new FormControl(''),
    admin_ID: new FormControl(''),
  });

  reset(id: number) {
    console.log(id)
    this.adminService.getToUpdateadmin(id)
      .subscribe(
        data => {
          this.adminToUpdate = data
        },
        error => console.log(error));
    if (this.resetform.get('admin_ID')?.value == this.adminToUpdate.admin_ID) {
      console.log('CONTINUE TO UPDATE');
      this.adminService.updateadmin1(this.adminToUpdate.admin_ID, this.resetform.value).subscribe(
        data => {
          this.isupdated = true;
          this.adminService.getAdmin().subscribe(data => {
            this.TAdmin = data
          })
        },
        error => console.log(error));
    }
  }
}
