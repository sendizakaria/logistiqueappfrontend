import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AppComponent} from 'src/app/app.component';
import {AdminService} from "../../services/admin.service";
import {Admin} from "../../interfaces/admin";
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-authentification',
  templateUrl: './authentification.component.html',
  styleUrls: ['./authentification.component.scss']
})
export class AuthentificationComponent implements OnInit {


  showlogin :boolean ;
  autentificationform!:FormGroup;
  admin: Admin[]=[]
  i!:number;
  valideMessage:string="" ;
  constructor(private route:ActivatedRoute,
              private router:Router,
              private appComponent: AppComponent,private adminService:AdminService ) {
    this.showlogin = this.appComponent.showlogin ? true:true;
  }

  ngOnInit(): void {
    this.autentificationform = new FormGroup({
      admin_EMAIL: new FormControl('',Validators.required),
      admin_PASSWORD: new FormControl('',Validators.required),   });

    console.log("ngOnInit MenuComponent this.appComponent.showlogin  :  "+this.appComponent.showlogin);
    this.showlogin = this.appComponent.showlogin ? true:true;
    console.log("ngOnInit MenuComponent this.appComponent.showlogin ||  :  "+this.appComponent.showlogin);
  }
  onSign(): void {
    if (this.autentificationform.valid){

    this.adminService.getAdmin().subscribe(
      data =>{this.admin = data;
      },
      err =>{throw new Error("please fill out the form before submitting")},
      ()=> console.log('AUTHENTIFIED SUCCEFULLY')
    )
    for( this.i=0;this.i<this.admin.length;this.i++)
    {
    if(this.autentificationform.get('admin_EMAIL')?.value==this.admin[this.i].admin_EMAIL && this.autentificationform.get('admin_PASSWORD')?.value==this.admin[this.i].admin_PASSWORD)
    {
     this.appComponent.showlogin = this.appComponent.showlogin ? false:false;
      this.router.navigate(['/welcome']);
    }
    }   }else {
      this.valideMessage="please fill out the form before submitting";
    }
  }}
