import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {HttpClient} from "@angular/common/http";
import {Chauffeur} from "../../interfaces/chauffeur";
import {DriverService} from "../../services/driver.service";

@Component({
  selector: 'app-auto',
  templateUrl: './drivercomponent.html',
  styleUrls: ['./drivercomponent.scss']
})
export class DriverComponent implements OnInit {

  deleteMessage=false;
  TDriver: Chauffeur[]=[];
  driverform!: FormGroup;
  chauffeurToUpdate!: Chauffeur;
  isupdated = false;
  validMessage: string = "";
  constructor(private http: HttpClient,private driverService:DriverService) { }

  ngOnInit(): void {
    this.driverform = new FormGroup({
      chauffeur_ID: new FormControl(''),
      chauffeur_NAME: new FormControl(''),
      chauffeur_LASTNAME: new FormControl(''),
      chauffeur_BYTE: new FormControl(''),
      chauffeur_BIRTH_DATE: new FormControl(''),
      chauffeur_LOGIN: new FormControl(''),
      chauffeur_PASSWORD: new FormControl(''),
      chauffeur_DATEENTREE: new FormControl(''),
      chauffeur_SPECIALITY: new FormControl(''),
      chauffeur_STATE: new FormControl(''),
    })
    this.driverService.getChauffeur().subscribe(data =>{
    })
    this.getallChauffeurs();
    console.log('all orders qre Loaded',this.TDriver);
  }
  getallChauffeurs(){
    this.driverService.getChauffeur().subscribe(
      data =>{this.TDriver = data},
      err =>console.error(err),
      ()=> console.log('all trips Loaded')
    )
  }
  enregistrerChauffeurs(){
    console.log('save client',this.driverform.value);
    if(this.driverform.valid ){
      console.log('suivieform valid',this.driverform.value);

      this.driverService.createChauffeur(this.driverform.value).subscribe(
        data=>{
          this.getallChauffeurs();
          return true;
          this.validMessage= "Your client registration has been submitted. \t Thank you ! ";
        },
        error=>{
          throw new Error("Please fill out the form before submitting !");  }
      )
    }else {
      this.validMessage= "Please fill out the form before submitting !";
    }
  }
  //update begain
  getToUpdateChauffeur(id: number) {
    this.driverService.getToUpdateChauffeur(id)
      .subscribe(
        data => {
          this.chauffeurToUpdate = data ,

            console.log(this.chauffeurToUpdate.chauffeur_ID)
        },
        error => console.log(error));
  }
  chauffeurupdateform = new FormGroup({
    chauffeur_ID: new FormControl(''),
    chauffeur_NAME: new FormControl(''),
    chauffeur_LASTNAME: new FormControl(''),
    chauffeur_BYTE: new FormControl(''),
    chauffeur_BIRTH_DATE: new FormControl(''),
    chauffeur_LOGIN: new FormControl(''),
    chauffeur_PASSWORD: new FormControl(''),
    chauffeur_DATEENTREE: new FormControl(''),
    chauffeur_SPECIALITY: new FormControl(''),
    chauffeur_STATE: new FormControl(''),
  });

  updateChauffeur(){
    console.log(this.chauffeurupdateform.value);
    this.driverService.updateChaffeur(this.chauffeurToUpdate.chauffeur_ID,this.chauffeurupdateform.value).subscribe(
      data => {
        this.isupdated=true;
        this.driverService.getChauffeur().subscribe(data =>{
          this.TDriver = data
        })
      },
      error => console.log(error));
  }
  get chauffeur_ID(){
    return this.chauffeurupdateform.get('chauffeur_ID');
  }
  get chauffeur_NAME(){
    return this.chauffeurupdateform.get('chauffeur_NAME');
  }
  get chauffeur_LASTNAME(){
    return this.chauffeurupdateform.get('chauffeur_LASTNAME');
  }
  get chauffeur_BYTE() {
    return this.chauffeurupdateform.get('chauffeur_BYTE');
  }
  get chauffeur_BIRTH_DATE(){
    return this.chauffeurupdateform.get('chauffeur_BIRTH_DATE');
  }

  get chauffeur_LOGIN(){
    return this.chauffeurupdateform.get('chauffeur_LOGIN');
  }
  get chauffeur_PASSWORD(){
    return this.chauffeurupdateform.get('chauffeur_PASSWORD');
  }
  get chauffeur_DATEENTREE(){
    return this.chauffeurupdateform.get('chauffeur_DATEENTREE');
  }
  get chauffeur_SPECIALITY(){
    return this.chauffeurupdateform.get('chauffeur_SPECIALITY');
  }
  get chauffeur_STATE(){
    return this.chauffeurupdateform.get('chauffeur_STATE');
  }
  changeisUpdate(){
    this.isupdated=false;
  }
//update end
  DeleteDriver(id: number) {
    this.driverService.deleteChauffeur(id)
      .subscribe(
        data => {
          console.log(data);
          this.deleteMessage=true;
          this.getallChauffeurs();
        },
        error => console.log(error));
  }

}
