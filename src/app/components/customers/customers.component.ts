import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {HttpClient} from "@angular/common/http";
import {Client} from "../../interfaces/client";
import {CustomerService} from "../../services/customer.service";


@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.scss']
})
export class CustomersComponent implements OnInit {
  deleteMessage = false;
  Tcustomers: Client[] = [];
  customerToUpdate!: Client;
  client!: Client;
  isupdated = false;
  customersform!: FormGroup;
  clientpdateform !: FormGroup;
  validMessage: string = "";

  //dtTrigger: Subject<any>= new Subject();
  constructor(private http: HttpClient, private customerService: CustomerService) {
  }

  ngOnInit(): void {
    this.isupdated = false;
    this.customersform = new FormGroup({
      client_ID: new FormControl(''),
      client_NAME: new FormControl(''),
      subscription_DATE: new FormControl(''),
      total_ORDERS: new FormControl(''),
      client_ADRESSE: new FormControl(''),
      client_TELE: new FormControl(''),
      client_ICE: new FormControl(''),
    })
    this.customerService.getClient().subscribe(data =>{
    //this.client= data;
    //this.dtTrigger.next();
    })
    this.getAllClients();
    console.log('all orders qre Loaded', this.Tcustomers);
  }

  getAllClients() {
    this.customerService.getClient().subscribe(
      data => {
        this.Tcustomers = data
      },
      err => console.error(err),
      () => console.log('all customers Loaded')
    )
  }

  enregistrerClient() {
    console.log('save client', this.Tcustomers);
    if (this.customersform.valid) {
      this.validMessage = "Your client registration has been submitted. \t Thank you ! ";
      this.customerService.createClients(this.customersform.value).subscribe(
        data => {
          this.customersform.reset();
          this.customerService.getClient().subscribe(data => {
            this.Tcustomers = data
          })
          return true;
        },
        error => {
          throw new Error("Please fill out the form before submitting !");
        }
      )
    } else {
      this.validMessage = "Please fill out the form before submitting !";

    }
  }

  //update begain
  getToUpdateClient(id: number) {
    this.customerService.getToUpdateClient(id)
      .subscribe(
        data => {
          this.customerToUpdate = data ,
         /*   console.log(this.customerToUpdate.client_ID),
            this.client.client_ID = this.customerToUpdate.client_ID,
            this.client.client_NAME = this.customerToUpdate.client_NAME,
            this.client.subscription_DATE = this.customerToUpdate.subscription_DATE,
            this.client.total_ORDERS = this.customerToUpdate.total_ORDERS, */
            console.log(this.customerToUpdate.total_ORDERS)
        },
        error => console.log(error));
  }

  clientupdateform = new FormGroup({
    client_ID: new FormControl(''),
    client_NAME: new FormControl(''),
    subscription_DATE: new FormControl(''),
    total_ORDERS: new FormControl(''),
  });

  updateclt(){
    console.log(this.clientupdateform.value);
    this.customerService.updateClient(this.customerToUpdate.client_ID,this.clientupdateform.value).subscribe(
      data => {
        this.isupdated=true;
        this.customerService.getClient().subscribe(data =>{
          this.Tcustomers = data
        })
      },
      error => console.log(error));
  }


  get client_ID(){
    return this.clientpdateform.get('client_ID');
  }

  get client_NAME(){
    return this.clientpdateform.get('client_NAME');
  }

  get subscription_DATE(){
    return this.clientpdateform.get('subscription_DATE');
  }

  get total_ORDERS(){
    return this.clientpdateform.get('total_ORDERS');
  }
  changeisUpdate(){
    this.isupdated=false;
  }
  //update end
  DeleteClients(id: number) {
    this.customerService.deleteClient(id)
      .subscribe(
        data => {
          console.log(data);
          this.deleteMessage=true;
          this.getAllClients();
        },
        error => console.log(error));
  }
}
