import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {HttpClient} from "@angular/common/http";
import {Order} from "../../interfaces/order";
import {OrderService} from "../../services/order.service";
import {Client} from "../../interfaces/client";

@Component({
  selector: 'app-voyages',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {
  deleteMessage=false;
  TOrder: Order[]=[];
  orderToUpdate!: Order;
  validMessage: string = "";
  orderform!: FormGroup;
  order!: Order;
  isupdated = false;
  constructor(private http: HttpClient,private orderService:OrderService) { }

  ngOnInit(): void {
    this.isupdated = false;
    this.orderform = new FormGroup({
      ord_id: new FormControl(''),
      ord_date: new FormControl(''),
      order_state: new FormControl(''),
      loading_place: new FormControl(''),
      unloading_place: new FormControl(''),
      car_type: new FormControl(''),
      order_client_name: new FormControl(''),
    })
    this.orderService.getOrder().subscribe(data =>{
    })
    this.getAllOrders();
    console.log('all orders qre Loaded',this.TOrder);
   }
  getAllOrders(){
    this.orderService.getOrder().subscribe(
      data =>{this.TOrder = data},
      err =>console.error(err),
      ()=> console.log('all trips Loaded')
    )
  }
  //update begain
  getToUpdateOrder(id: number) {
    this.orderService.getToUpdateOrders(id)
      .subscribe(
        data => {
          this.orderToUpdate = data ,

            console.log(this.orderToUpdate.order_state)
        },
        error => console.log(error));
  }
  ordersupdateform = new FormGroup({
    ord_id: new FormControl(''),
    ord_date: new FormControl(''),
    order_state: new FormControl(''),
    loading_place: new FormControl(''),
    unloading_place: new FormControl(''),
    car_type: new FormControl(''),
    order_client_name: new FormControl(''),
});

updateOrder(){
  console.log(this.ordersupdateform.value);
  this.orderService.updateOrder(this.orderToUpdate.ord_id,this.ordersupdateform.value).subscribe(
    data => {
      this.isupdated=true;
      this.orderService.getOrder().subscribe(data =>{
        this.TOrder = data
      })
    },
    error => console.log(error));
}
get ord_id(){
  return this.ordersupdateform.get('ord_id');
}

get ord_date(){
  return this.ordersupdateform.get('ord_date');
}

get order_state(){
  return this.ordersupdateform.get('order_state');
}

get loading_place() {
  return this.ordersupdateform.get('loading_place');
}
  get unloading_place(){
    return this.ordersupdateform.get('unloading_place');
  }

  get car_type(){
    return this.ordersupdateform.get('car_type');
  }

  get order_client_name(){
    return this.ordersupdateform.get('order_client_name');
}
changeisUpdate(){
  this.isupdated=false;
}
//update end
  enregistrerOrder(){
    console.log('save client',this.TOrder);
    if(this.orderform.valid ){
      this.validMessage= "Your client registration has been submitted. \t Thank you ! ";
      this.orderService.createOrder(this.orderform.value).subscribe(
        data=>{
          this.orderform.reset();
          this.orderService.getOrder().subscribe(data =>{
            this.TOrder =data
          })
          return true;
        },
        error=>{
          throw new Error("Please fill out the form before submitting !");  }
      )
    }else {
      this.validMessage= "Please fill out the form before submitting !";

    }
  }
  Deleteorder(id: number) {
    this.orderService.deleteOrder(id)
      .subscribe(
        data => {
          console.log(data);
          this.deleteMessage=true;
          this.getAllOrders();
        },
        error => console.log(error));
  }
}
