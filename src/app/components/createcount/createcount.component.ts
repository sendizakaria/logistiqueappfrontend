import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {HttpClient} from "@angular/common/http";
import {AdminService} from "../../services/admin.service";
import {Admin} from "../../interfaces/admin";

@Component({
  selector: 'app-createcount',
  templateUrl: './createcount.component.html',
  styleUrls: ['./createcount.component.scss']
})
export class CreatecountComponent implements OnInit {
  userupdateform!: FormGroup;
  isupdated = false;
  deleteMessage=false;
  TAdmin: Admin[]=[];
  chauffeurToUpdate!: Admin;
  validMessage: string = "";
  constructor(private http: HttpClient,private adminService:AdminService) { }
  ngOnInit(): void {
    this.userupdateform = new FormGroup({
      admin_ID: new FormControl(''),
      admin_NAME: new FormControl(''),
      admin_LASTNAME: new FormControl(''),
      admin_BIRTHDATE:new FormControl(''),
      admin_GENDER: new FormControl(''),
      admin_ADRESSE:new FormControl(''),
      admin_NUM:new FormControl(''),
      admin_TELE: new FormControl(''),
      admin_EMAIL:new FormControl(''),
      admin_BYTE:new FormControl(''),
      admin_LOGIN: new FormControl(''),
      admin_PASSWORD: new FormControl(''),
    });
  }
  getalladmin(){
    this.adminService.getAdmin().subscribe(
      data =>{this.TAdmin = data},
      err =>console.error(err),
      ()=> console.log('all trips Loaded')
    )
  }
 adduser(){
    console.log('save user',this.userupdateform.value);
    if(this.userupdateform.valid ){
      console.log('suivieform valid',this.userupdateform.value);
      this.adminService.createAdmin(this.userupdateform.value).subscribe(
        data=>{
          this.getalladmin();
          this.isupdated=true;
          return true;
          this.validMessage= "Your client registration has been submitted. \t Thank you ! ";
        },
        error=>{
          throw new Error("Please fill out the form before submitting !");  }
      )
    }else {
      this.validMessage= "Please fill out the form before submitting !";
    }
  }

}
