import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatecountComponent } from './createcount.component';

describe('CreatecountComponent', () => {
  let component: CreatecountComponent;
  let fixture: ComponentFixture<CreatecountComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreatecountComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatecountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
