import { Component, OnInit } from '@angular/core';
import {Client} from "../../interfaces/client";
import {FormControl, FormGroup} from "@angular/forms";
import {HttpClient} from "@angular/common/http";
import {CustomerService} from "../../services/customer.service";
import {Charges} from "../../interfaces/charges";
import {ChargeService} from "../../services/charge.service";
import {Facture} from "../../interfaces/facture";

@Component({
  selector: 'app-cars-charges',
  templateUrl: './charges.component.html',
  styleUrls: ['./charges.component.scss']
})
export class ChargesComponent implements OnInit {

  deleteMessage=false;
  Tcharges: Charges[]=[];
  TchargeS!: Charges;
  chargesform!: FormGroup;
  validMessage: string = "";
  chargeToUpdate!: Charges;
  isupdated = false;
  constructor(private http: HttpClient,private chargeService:ChargeService) { }

  ngOnInit(): void {
    this.chargesform = new FormGroup({
      charge_ID: new FormControl(''),
      charge_TYPE: new FormControl(''),
      charge_DATE: new FormControl(''),
      charge_STATE: new FormControl(''),
      charge_PRICE: new FormControl(''),
    })
    this.chargeService.getCharges().subscribe(data =>{
    })
    this.getAllCharges();
    console.log('all orders qre Loaded',this.Tcharges);
  }
  getAllCharges(){
    this.chargeService.getCharges().subscribe(
      data =>{this.Tcharges = data},
      err =>console.error(err),
      ()=> console.log('all customers Loaded')
    )
  }

  enregistrerCharges(){
    console.log('save client',this.chargesform.value);
    if(this.chargesform.valid ){
      console.log('suivieform valid',this.chargesform.value);

      this.chargeService.createCharge(this.chargesform.value).subscribe(
        data=>{
          this.getAllCharges();
          return true;
          this.validMessage= "Your client registration has been submitted. \t Thank you ! ";
        },
        error=>{
          throw new Error("Please fill out the form before submitting !");  }
      )
    }else {
      this.validMessage= "Please fill out the form before submitting !";
    }
  }
  //update begain
  getToUpdateCharge(id: number) {
    this.chargeService.getToUpdatecharge(id)
      .subscribe(
        data => {
          this.chargeToUpdate = data ,
            console.log(this.chargeToUpdate.charge_ID)
        },
        error => console.log(error));
  }
  chargeupdateform = new FormGroup({
    charge_ID: new FormControl(''),
    charge_TYPE: new FormControl(''),
    charge_DATE: new FormControl(''),
    charge_STATE: new FormControl(''),
    charge_PRICE: new FormControl(''),
  });

  updateCharge(){
    console.log(this.chargeupdateform.value);
    this.chargeService.updatecharge(this.chargeToUpdate.charge_ID,this.chargeupdateform.value).subscribe(
      data => {
        this.isupdated=true;
        this.chargeService.getCharges().subscribe(data =>{
          this.Tcharges = data
        })
      },
      error => console.log(error));
  }
  get charge_ID(){
    return this.chargeupdateform.get('charge_ID');
  }
  get charge_TYPE(){
    return this.chargeupdateform.get('charge_TYPE');
  }
  get charge_DATE(){
    return this.chargeupdateform.get('charge_DATE');
  }
  get charge_STATE() {
    return this.chargeupdateform.get('charge_STATE');
  }
  get charge_PRICE(){
    return this.chargeupdateform.get('charge_PRICE');
  }
  changeisUpdate(){
    this.isupdated=false;
  }
//update end
  DeleteCharges(id: number) {
    this.chargeService.deleteCharges(id)
      .subscribe(
        data => {
          console.log(data);
          this.deleteMessage=true;
          this.getAllCharges();
        },
        error => console.log(error));
  }

}
