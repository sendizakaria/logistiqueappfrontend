import {Component, OnInit} from '@angular/core';
import {AppComponent} from 'src/app/app.component';
import {ActivatedRoute, Router} from '@angular/router';
import {FormControl, FormGroup} from "@angular/forms";
import {Admin} from "../../interfaces/admin";
import {AdminService} from "../../services/admin.service";

@Component({
  selector: 'app-lock',
  templateUrl: './lock.component.html',
  styleUrls: ['./lock.component.scss']
})
export class LockComponent implements OnInit {


  lockform!:FormGroup;
  admin: Admin[]=[]
  showlogin :boolean ;
  i!:number;
  constructor(   private appComponent: AppComponent,private route:ActivatedRoute,
    private router:Router,private adminService:AdminService) {   this.showlogin = this.appComponent.showlogin ? true:true;}

  ngOnInit(): void {
    this.lockform = new FormGroup({
      admin_LOGIN: new FormControl(''),
        });

    console.log("ngOnInit MenuComponent this.appComponent.showlogin  :  "+this.appComponent.showlogin);
    this.showlogin = this.appComponent.showlogin ? true:true;
    console.log("ngOnInit MenuComponent this.appComponent.showlogin ||  :  "+this.appComponent.showlogin);
  }
  unlock(): void {
    this.adminService.getAdmin().subscribe(
      data =>{this.admin = data;
      },
      err =>console.error(err),
      ()=> console.log('Patients Loaded')
    )
    for( this.i=0;this.i<this.admin.length;this.i++)
    {
      if(this.lockform.get('admin_LOGIN')?.value==this.admin[this.i].admin_LOGIN)
      {
        this.appComponent.showlogin = this.appComponent.showlogin ? false:false;
        this.router.navigate(['/welcome']);
      }
    }
    }
    }

