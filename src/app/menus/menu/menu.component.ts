import { Component, OnInit } from '@angular/core';
import {AppComponent} from "../../app.component";
import {ActivatedRoute, Router} from "@angular/router";
import {Admin} from "../../interfaces/admin";
import {AdminService} from "../../services/admin.service";
import {FormGroup} from "@angular/forms";

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})

export class MenuComponent implements OnInit {
  showlogin :boolean=true;
  admin1   !: Admin
  adminform!:FormGroup;

  constructor(private route:ActivatedRoute,
    private router:Router,  private appComponent: AppComponent,private adminService:AdminService ) {
    }
  ngOnInit(): void {

  }
  lock():void
  {
    this.appComponent.showlogin = this.appComponent.showlogin ? true:true;
    this.router.navigate(['/lock']);
  }
  reset():void
  {
    this.appComponent.showlogin = this.appComponent.showlogin ? true:true;
    this.router.navigate(['/reset']);
  }

  map():void
  {
    this.appComponent.showlogin = this.appComponent.showlogin ? false:false;
    this.router.navigate(['/map']);
  }
  chart():void
  {
    this.appComponent.showlogin = this.appComponent.showlogin ? false:false;
    this.router.navigate(['/chart']);
  }
  welcome():void
  {
    this.appComponent.showlogin = this.appComponent.showlogin ? false:false;
    this.router.navigate(['/welcome']);

  }
  voyages():void
  {
    this.appComponent.showlogin = this.appComponent.showlogin ? false:false;
    this.router.navigate(['/voyages']);
  }
  suivie():void
  {
    this.appComponent.showlogin = this.appComponent.showlogin ? false:false;
    this.router.navigate(['/suivie']);
  }
  facture():void
  {
    this.appComponent.showlogin = this.appComponent.showlogin ? false:false;
    this.router.navigate(['/facture']);
  }

  profile():void
  {
    this.appComponent.showlogin = this.appComponent.showlogin ? false:false;
    this.router.navigate(['/profile']);
  }
  clients():void
  {
    this.appComponent.showlogin = this.appComponent.showlogin ? false:false;
    this.router.navigate(['/customers']);
  }
  driver():void
  {
    this.appComponent.showlogin = this.appComponent.showlogin ? false:false;
    this.router.navigate(['/driver']);
  }
  autos():void
  {
    this.appComponent.showlogin = this.appComponent.showlogin ? false:false;
    this.router.navigate(['/autos']);
  }
  charges():void
  {
    this.appComponent.showlogin = this.appComponent.showlogin ? false:false;
    this.router.navigate(['/charges']);
  }
  calendrier():void
  {
    this.appComponent.showlogin = this.appComponent.showlogin ? false:false;
    this.router.navigate(['/calendrier']);
  }
  messagerie():void
  {
    this.appComponent.showlogin = this.appComponent.showlogin ? false:false;
    this.router.navigate(['/messagerie']);
  }

}
