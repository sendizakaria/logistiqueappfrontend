import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {catchError, map, Observable, tap, throwError} from 'rxjs';
import {Order} from "../interfaces/order";
import {Client} from "../interfaces/client";
import {Facture} from "../interfaces/facture";

const httpOptions = {
  headers: new HttpHeaders({'Content-Type':'application/json'})
 }

@Injectable({
providedIn:'root'
})
export class OrderService {
  urlorder: string = '/server/v1/orders';

  constructor(private http: HttpClient) {
  }

  getOrder(): Observable<Order[]> {
    return this.http.get<Order[]>(this.urlorder +'/orders-list').pipe(
      tap(data => console.log('All trips', JSON.stringify(data)), catchError(this.handleError)));
  }

  getOrderById(id : number) : Observable<Order  | undefined> {
    return this.getOrder().pipe(
      map((bike: Order[]) => bike.find(p => p.ord_id === id))
    );
  }

  createOrder(clt :Order)  {
    return this.http.post<Order[]>(this.urlorder+"/save-orders",JSON.stringify(clt), httpOptions);
  }

  deleteOrder(id:number): Observable<any> {
    return this.http.delete(`${this.urlorder}/delete-order/${id}`, { responseType: 'text' });
  }

  getToUpdateOrders(id: number) : Observable<Order> {
    return this.http.get<Order>(`${this.urlorder}/${id}`).pipe(
      tap(data => console.log('One', JSON.stringify(data)))) ;
  }
  updateOrder(id: number, value: any): Observable<Object> {
    return this.http.post(`${this.urlorder}/update-order/${id}`, value);
  }
  handleError(handleError: HttpErrorResponse) {
    let errorMessage;

    if (handleError.error instanceof ErrorEvent) {
      errorMessage = `error occured ${handleError.error.message}`;
    } else {
      errorMessage = `Server returned Code ${handleError.status} , error message is ${handleError.error.message}`;
    }

    console.log(errorMessage);
    return throwError(errorMessage);

  }
}
