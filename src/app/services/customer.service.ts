import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import {catchError, Observable, tap, throwError} from "rxjs";
import {Client} from "../interfaces/client";
import {Facture} from "../interfaces/facture";

const httpOptions = {
  headers: new HttpHeaders({'Content-Type':'application/json'})
}
@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  urlcustomer: string = '/server/v1/client';

  constructor(private http: HttpClient) { }

  getClient() : Observable<Client[]> {
    return this.http.get<Client[]>(this.urlcustomer+'/clients-list').pipe(
      tap(data => console.log('All trips', JSON.stringify(data)), catchError(this.handleError))) ;
  }

  createClients(clt :Client)  {
    return this.http.post<Client[]>(this.urlcustomer+"/save-client",JSON.stringify(clt), httpOptions);
  }
  deleteClient(id:number): Observable<any> {
    return this.http.delete(`${this.urlcustomer}/delete-client/${id}`, { responseType: 'text' });
  }

  getToUpdateClient(id: number) : Observable<Client> {
    return this.http.get<Client>(`${this.urlcustomer}/${id}`).pipe(
      tap(data => console.log('One', JSON.stringify(data)))) ;
  }

  getToshowClient(name: string) : Observable<Client> {
    return this.http.get<Client>(`${this.urlcustomer}/showclient/${name}`).pipe(
      tap(data => console.log('One', JSON.stringify(data)))) ;
  }
  updateClient(id: number, value: any): Observable<Object> {
    return this.http.post(`${this.urlcustomer}/update-client/${id}`, value);
  }





  handleError(handleError: HttpErrorResponse) {
    let errorMessage ;

    if(handleError.error instanceof ErrorEvent)    {
      errorMessage =`error occured ${handleError.error.message}`;
    }
    else {
      errorMessage =`Server returned Code ${handleError.status} , error message is ${handleError.error.message}`;
    }

    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
