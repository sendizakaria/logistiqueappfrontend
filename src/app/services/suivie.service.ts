import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import {catchError, Observable, tap, throwError} from "rxjs";
import {Client} from "../interfaces/client";
import {Suivie} from "../interfaces/suivie";
import {Order} from "../interfaces/order";

const httpOptions = {
  headers: new HttpHeaders({'Content-Type':'application/json'})
}
@Injectable({
  providedIn: 'root'
})
export class SuivieService {
  urlsuivie: string = 'server/v1/suivie';

  constructor(private http: HttpClient) { }

  getSuivie() : Observable<Suivie[]> {
    return this.http.get<Suivie[]>(this.urlsuivie+'/suivie-list').pipe(
      tap(data => console.log('All trips', JSON.stringify(data)), catchError(this.handleError))) ;
  }

  deleteSuivie(id:number): Observable<any> {
    return this.http.delete(`${this.urlsuivie}/delete-suivie/${id}`, { responseType: 'text' });
  }

  createSuivie(suivie :Suivie)  {
    return this.http.post<Suivie[]>(this.urlsuivie+'/save-suivie',JSON.stringify(suivie), httpOptions);
  }
  getToUpdateSuivie(id: number) : Observable<Suivie> {
    return this.http.get<Suivie>(`${this.urlsuivie}/${id}`).pipe(
      tap(data => console.log('One', JSON.stringify(data)))) ;
  }
  updateSuivie(id: number, value: any): Observable<Object> {
    return this.http.post(`${this.urlsuivie}/update-suivie/${id}`, value);
  }
  handleError(handleError: HttpErrorResponse) {
    let errorMessage ;

    if(handleError.error instanceof ErrorEvent)    {
      errorMessage =`error occured ${handleError.error.message}`;
    }
    else {
      errorMessage =`Server returned Code ${handleError.status} , error message is ${handleError.error.message}`;
    }

    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
