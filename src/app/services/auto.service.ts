import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import {catchError, Observable, tap, throwError} from "rxjs";
import {Vheicule} from "../interfaces/vheicule";
import {Chauffeur} from "../interfaces/chauffeur";
import {Charges} from "../interfaces/charges";

const httpOptions = {
  headers: new HttpHeaders({'Content-Type':'application/json'})
}
@Injectable({
  providedIn: 'root'
})
export class AutoService {
  urlauto: string = 'server/v1/vheicule';

  constructor(private http: HttpClient) { }

  getAuto() : Observable<Vheicule[]> {
    return this.http.get<Vheicule[]>(this.urlauto+'/vheicule-list').pipe(
      tap(data => console.log('All trips', JSON.stringify(data)), catchError(this.handleError))) ;
  }

  createAuto(vheicule :Vheicule)  {
    return this.http.post<Vheicule[]>(this.urlauto+'/save-vheicule',JSON.stringify(vheicule), httpOptions);
  }
  deleteAuto(id:number): Observable<any> {
    return this.http.delete(`${this.urlauto}/delete-vheicule/${id}`, { responseType: 'text' });
  }
  getToUpdateauto(id: number) : Observable<Vheicule> {
    return this.http.get<Vheicule>(`${this.urlauto}/${id}`).pipe(
      tap(data => console.log('One', JSON.stringify(data)))) ;
  }
  updateAuto(id: number, value: any): Observable<Object> {
    return this.http.post(`${this.urlauto}/update-vheicule/${id}`, value);
  }


  handleError(handleError: HttpErrorResponse) {
    let errorMessage ;

    if(handleError.error instanceof ErrorEvent)    {
      errorMessage =`error occured ${handleError.error.message}`;
    }
    else {
      errorMessage =`Server returned Code ${handleError.status} , error message is ${handleError.error.message}`;
    }

    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
