import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import {catchError, map, Observable, tap, throwError} from "rxjs";
import {Facture} from "../interfaces/facture";
import {Suivie} from "../interfaces/suivie";
import {Chauffeur} from "../interfaces/chauffeur";

const httpOptions = {
  headers: new HttpHeaders({'Content-Type':'application/json'})
}
@Injectable({
  providedIn: 'root'
})
export class FactureService {
  urlfacture: string = '/server/v1/facture';

  constructor(private http: HttpClient) { }

  getFacture() : Observable<Facture[]> {
    return this.http.get<Facture[]>(this.urlfacture+'/facture-list').pipe(
      tap(data => console.log('All trips', JSON.stringify(data)), catchError(this.handleError))) ;
  }

  createFacture(facture :Facture)  {
    return this.http.post<Facture[]>(this.urlfacture+'/save-facture',JSON.stringify(facture), httpOptions);
  }
  deleteFacture(id:number): Observable<any> {
    return this.http.delete(`${this.urlfacture}/delete-facture/${id}`, { responseType: 'text' });
  }
  getToUpdatefacture(id: number) : Observable<Facture> {
    return this.http.get<Facture>(`${this.urlfacture}/${id}`).pipe(
      tap(data => console.log('One', JSON.stringify(data)))) ;
  }

  updatefacture(id: number, value: any): Observable<Object> {
    return this.http.post(`${this.urlfacture}/update-facture/${id}`, value);
  }

  getFactureById(id : number) : Observable<Facture  | undefined> {
    return this.getFacture().pipe(
      map((bike: Facture[]) => bike.find(p => p.facture_ID === id))
    );
  }

  handleError(handleError: HttpErrorResponse) {
    let errorMessage ;

    if(handleError.error instanceof ErrorEvent)    {
      errorMessage =`error occured ${handleError.error.message}`;
    }
    else {
      errorMessage =`Server returned Code ${handleError.status} , error message is ${handleError.error.message}`;
    }

    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
