import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import {catchError, Observable, tap, throwError} from "rxjs";
import {Charges} from "../interfaces/charges";
import {Facture} from "../interfaces/facture";

const httpOptions = {
  headers: new HttpHeaders({'Content-Type':'application/json'})
}
@Injectable({
  providedIn: 'root'
})
export class ChargeService {
  urlcharge: string = '/server/v1/charges';

  constructor(private http: HttpClient) { }

  getCharges() : Observable<Charges[]> {
    return this.http.get<Charges[]>(this.urlcharge+'/charges-list').pipe(
      tap(data => console.log('All trips', JSON.stringify(data)), catchError(this.handleError))) ;
  }

  createCharge(charges :Charges)  {
    return this.http.post<Charges[]>(this.urlcharge+'/save-charges',JSON.stringify(charges), httpOptions);
  }
  deleteCharges(id:number): Observable<any> {
    return this.http.delete(`${this.urlcharge}/delete-charges/${id}`, { responseType: 'text' });
  }
  getToUpdatecharge(id: number) : Observable<Charges> {
    return this.http.get<Charges>(`${this.urlcharge}/${id}`).pipe(
      tap(data => console.log('One', JSON.stringify(data)))) ;
  }
  updatecharge(id: number, value: any): Observable<Object> {
    return this.http.post(`${this.urlcharge}/update-charges/${id}`, value);
  }


  handleError(handleError: HttpErrorResponse) {
    let errorMessage ;

    if(handleError.error instanceof ErrorEvent)    {
      errorMessage =`error occured ${handleError.error.message}`;
    }
    else {
      errorMessage =`Server returned Code ${handleError.status} , error message is ${handleError.error.message}`;
    }

    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
