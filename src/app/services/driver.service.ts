import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import {catchError, Observable, tap, throwError} from "rxjs";
import {Order} from "../interfaces/order";
import {Chauffeur} from "../interfaces/chauffeur";
import {Charges} from "../interfaces/charges";
import {Suivie} from "../interfaces/suivie";

const httpOptions = {
  headers: new HttpHeaders({'Content-Type':'application/json'})
}
@Injectable({
  providedIn: 'root'
})
export class DriverService {
  urldriver: string = '/server/v1/chauffeur';

  constructor(private http: HttpClient) {
  }

  getChauffeur(): Observable<Chauffeur[]> {
    return this.http.get<Chauffeur[]>(this.urldriver +'/chauffeur-list').pipe(
      tap(data => console.log('All trips', JSON.stringify(data)), catchError(this.handleError)));
  }


  createChauffeur(chauffeur :Chauffeur)  {
    return this.http.post<Chauffeur[]>(this.urldriver+'/save-chauffeur',JSON.stringify(chauffeur), httpOptions);
  }
  getToUpdateChauffeur(id: number) : Observable<Chauffeur> {
    return this.http.get<Chauffeur>(`${this.urldriver}/${id}`).pipe(
      tap(data => console.log('One', JSON.stringify(data)))) ;
  }
  updateChaffeur(id: number, value: any): Observable<Object> {
    return this.http.post(`${this.urldriver}/update-chauffeur/${id}`, value);
  }
  deleteChauffeur(id:number): Observable<any> {
    return this.http.delete(`${this.urldriver}/delete-chauffeur/${id}`, { responseType: 'text' });
  }
  handleError(handleError: HttpErrorResponse) {
    let errorMessage;

    if (handleError.error instanceof ErrorEvent) {
      errorMessage = `error occured ${handleError.error.message}`;
    } else {
      errorMessage = `Server returned Code ${handleError.status} , error message is ${handleError.error.message}`;
    }

    console.log(errorMessage);
    return throwError(errorMessage);

  }
}
