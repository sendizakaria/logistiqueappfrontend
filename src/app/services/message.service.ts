import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import {Message} from "../interfaces/message";
import {catchError, Observable, tap, throwError} from "rxjs";
import {Facture} from "../interfaces/facture";
const httpOptions = {
  headers: new HttpHeaders({'Content-Type':'application/json'})
}
@Injectable({
  providedIn: 'root'
})
export class MessageService {
  urlmessage: string = '/server/v1/message'
  constructor(private http: HttpClient) { }
  getMessage() : Observable<Message[]> {
    return this.http.get<Message[]>(this.urlmessage+'/message-list').pipe(
      tap(data => console.log('All trips', JSON.stringify(data)), catchError(this.handleError))) ;
  }
  createMessage(message :Message)  {
    return this.http.post<Message[]>(this.urlmessage+'/save-message',JSON.stringify(message), httpOptions);
  }
  handleError(handleError: HttpErrorResponse) {
    let errorMessage ;

    if(handleError.error instanceof ErrorEvent)    {
      errorMessage =`error occured ${handleError.error.message}`;
    }
    else {
      errorMessage =`Server returned Code ${handleError.status} , error message is ${handleError.error.message}`;
    }

    console.log(errorMessage);
    return throwError(errorMessage);
  }

}
