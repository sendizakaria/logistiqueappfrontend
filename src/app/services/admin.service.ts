import { Injectable } from '@angular/core';
import {Admin} from "../interfaces/admin";
import {catchError, Observable, tap, throwError} from "rxjs";
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import {Charges} from "../interfaces/charges";
import {Chauffeur} from "../interfaces/chauffeur";

const httpOptions = {
  headers: new HttpHeaders({ 'Accept': 'application/json','Content-Type':'application/json'}  )
}

@Injectable({
  providedIn: 'root'
})
export class AdminService {
  urlAdmin: string = '/server/v1/admin';
  constructor(private http: HttpClient ) { }
  getAdmin() : Observable<Admin[]> {
    return this.http.get<Admin[]>(this.urlAdmin+'/admin-list').pipe(
      tap(data => console.log('All', JSON.stringify(data)), catchError(this.handleError))) ;
  }
  createAdmin(admin :Admin)  {
    return this.http.post<Admin[]>(this.urlAdmin+'/save-admin',JSON.stringify(admin), httpOptions);
  }
  getToUpdateadmin(id: number) : Observable<Admin> {
    return this.http.get<Admin>(`${this.urlAdmin}/${id}`).pipe(
      tap(data => console.log('One', JSON.stringify(data)))) ;
  }
  updateadmin(id: number, value: any): Observable<Object> {
    return this.http.post(`${this.urlAdmin}/update-admin/${id}`, value);
  }
  updateadmin1(id: number, value: any): Observable<Object> {
    return this.http.post(`${this.urlAdmin}/update-admin1/${id}`, value);
  }
  handleError(handleError: HttpErrorResponse) {
    let errorMessage ='';

    if(handleError.error instanceof ErrorEvent)    {
      errorMessage =`error occured ${handleError.error.message}`;
    }
    else {
      errorMessage =`Server returned Code ${handleError.status} , error message is ${handleError.error.message}`;
    }

    console.log(errorMessage);

    return throwError(errorMessage);
  }
}
