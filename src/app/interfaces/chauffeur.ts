export interface Chauffeur {
  chauffeur_ID:number;
  chauffeur_NAME:string;
  chauffeur_LASTNAME:string;
  chauffeur_BYTE:any[];
  chauffeur_BIRTH_DATE:Date;
  chauffeur_LOGIN:string;
  chauffeur_PASSWORD:string;
  chauffeur_DATEENTREE:Date;
  chauffeur_SPECIALITY:string;
  chauffeur_STATE:string;
}
