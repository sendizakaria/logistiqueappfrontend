export interface Order {
  ord_date:string;
  ord_id:number;
  order_state:string;
  loading_place:string;
  unloading_place:string;
  car_type:string;
  order_client_name:string;

}
