import {Time} from "highcharts";

export interface Message {
  message_id:number;
  chauffeur_NAME:string;
  admin_ID:number;
  message_TEXT:string;
  message_DATE:Date;
  message_TIME:Time;
}
