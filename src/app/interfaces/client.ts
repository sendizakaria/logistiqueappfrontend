export interface Client {

     client_ID:number;
     client_NAME:string;
     subscription_DATE:string;
     client_ADRESSE:string;
     client_TELE:string;
     client_ICE:number;
     total_ORDERS:string;
}
