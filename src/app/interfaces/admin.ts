export interface Admin {
  admin_ID: number ;
  admin_NAME:string;
  admin_LASTNAME: string ;
  admin_BIRTHDATE:Date;
  admin_GENDER: string ;
  admin_ADRESSE:string;
  admin_NUM:number;
  admin_TELE: string ;
  admin_EMAIL:string;
  admin_BYTE:any[];
  admin_LOGIN:string;
  admin_PASSWORD: string ;
}
