import {Time} from "@angular/common";

export interface Suivie {
  suivie_ID:number;
  suivie_CLIENT_NAME:string;
  driver_NAME:string;
  driver_GSM:string;
  suivie_Ville_DEPART:string;
  suivie_Ville_DESTINATION:string;
  suivie_DATE_Depart:Date;
  suivie_DATE_Arrivee:Date;
  dispo_CMR:string;
  suivie_State:string;
  arrival_TIME_LOADING:string;
  loading_TIME:string;
  arrival_TIME_UNLOADING:string;
  unloading_TIME:string;
  state_CMR:string;
}
