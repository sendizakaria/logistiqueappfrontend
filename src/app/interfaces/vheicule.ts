export interface Vheicule {
     vheicule_ID:number;
  vheicule_BRAND:string;
  vheicule_MODEL:string;
  vheicule_TYPE:string;
  vheicule__BYTE:any[];
  vheicule_STATE:String;
}
