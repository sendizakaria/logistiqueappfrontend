export interface Charges {
  charge_ID:number;
  charge_TYPE:string;
  charge_DATE:Date;
  charge_STATE:string;
  charge_PRICE:number;
}

