export interface Facture {
    facture_ID:number;
    order_ID:number;
    company_NAME:string;
    order_DATE_EDITION:Date;
    facture_DATE:Date;
    facture_TOTALE:number;
    facture_STATE:string;
    fac_BYTE:any[];
}
