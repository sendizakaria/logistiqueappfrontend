import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {LoginComponent} from './menus/login/login.component';
import {MenuComponent} from './menus/menu/menu.component';
import {ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {HttpClientModule} from "@angular/common/http";
import {LockComponent} from './components/lock/lock.component';
import {AuthentificationComponent} from './components/authentification/authentification.component';
import {LostpasswordComponent} from './components/lostpassword/lostpassword.component';
import {CreatecountComponent} from './components/createcount/createcount.component';
import {ResetComponent} from './components/reset/reset.component';
import {MapComponent} from './components/map/map.component';
import {ChartsComponent} from './components/charts/charts.component';
import {CalendarComponent} from './components/calendar/calendar.component';
import {WelcomeComponent} from './components/welcome/welcome.component';
import {SuivieComponent} from './components/suivie/suivie.component';
import {FactureComponent} from './components/facture/facture.component';
import {ProfileComponent} from './components/profile/profile.component';
import {CreeFComponent} from './components/cree-f/cree-f.component';
import {CustomersComponent} from './components/customers/customers.component';
import {AutosComponent} from './components/autos/autos.component';
import {OrdersComponent} from "./components/orders/orders.component";
import {OrderService} from "./services/order.service";
import {DriverComponent} from "./components/driver/driver.component";
import {ChargesComponent} from "./components/charges/charges.component";
import {HighchartsChartModule} from "highcharts-angular";
import { MessagerieComponent } from './components/messagerie/messagerie.component';
import { FullCalendarModule } from '@fullcalendar/angular';
import dayGridPlugin from '@fullcalendar/daygrid';
import interactionPlugin from '@fullcalendar/interaction';
import { MessageComponent } from './components/message/message.component';


FullCalendarModule.registerPlugins([
  dayGridPlugin,
  interactionPlugin
]);
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MenuComponent,
    LockComponent,
    AuthentificationComponent,
    LostpasswordComponent,
    ResetComponent,
    MapComponent,
    ChartsComponent,
    CalendarComponent,
    WelcomeComponent,
    OrdersComponent,
    SuivieComponent,
    FactureComponent,
    ProfileComponent,
    CreeFComponent,
    CustomersComponent,
    DriverComponent,
    AutosComponent,
    ChargesComponent,
    CreatecountComponent,
    MessagerieComponent,
    MessageComponent,


  ],

  imports: [
    BrowserModule,
    CommonModule,
    AppRoutingModule,
    HttpClientModule,
    HighchartsChartModule,
    RouterModule.forRoot([
      {
        path: '', component: AuthentificationComponent
      },
      {path: 'menu', component: MenuComponent},
      {path: 'welcome', component: WelcomeComponent},
      {path: 'lock', component: LockComponent},
      {path: 'lostpassword', component: LostpasswordComponent},
      {path: 'createcount', component: CreatecountComponent},
      {path: 'reset', component: ResetComponent},
      {path: 'map', component: MapComponent},
      {path: 'chart', component: ChartsComponent},
      {path: 'voyages', component: OrdersComponent},
      {path: 'suivie', component: SuivieComponent},
      {path: 'facture', component: FactureComponent},
      {path: 'profile', component: ProfileComponent},
      {path: 'cree-f', component: CreeFComponent},
      {path: 'cree-f/:id', component: CreeFComponent},
      {path: 'customers', component: CustomersComponent},
      {path: 'driver', component: DriverComponent},
      {path: 'autos', component: AutosComponent},
      {path: 'charges', component: ChargesComponent},
      {path: 'calendrier', component: CalendarComponent},
      {path: 'messagerie', component: MessagerieComponent},
      {path: 'message', component: MessageComponent},
    ]),
    ReactiveFormsModule,
    FullCalendarModule
  ],
  providers: [OrderService],
  bootstrap: [AppComponent]
})
export class AppModule { }
